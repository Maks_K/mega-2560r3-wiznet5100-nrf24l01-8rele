/**
* read_EEPROM_Settings function
* This function is used to read the EEPROM settings at startup
*
* Overview:
* - Set the PIN for the RESET-button to input and activate pullups
* - Load the stored data from EEPROM into the eeprom_config struct
* - Check if a config is stored or the reset button is pressed. If one of the conditions is ture, set the defaults
*/
void read_EEPROM_Settings() {
//  pinMode(RESET_PIN, INPUT);
//  digitalWrite(RESET_PIN, HIGH);
  
  // read the current config
  EEPROM_readAnything(2, eeprom_config);
 // EEPROM_readAnything(Adr_Save_EEPROM_Rele, eeprom_config_pin);

  
  // check if config is present or if reset button is pressed
//  if (eeprom_config.config_set != 1 || digitalRead(RESET_PIN) == LOW) {
  if (eeprom_config.config_set != 1) {
  // set default values
    set_EEPROM_Default();
    
    // write the config to eeprom
    EEPROM_writeAnything(2, eeprom_config);
  } 
}
