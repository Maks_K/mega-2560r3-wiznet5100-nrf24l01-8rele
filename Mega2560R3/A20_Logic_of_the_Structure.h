#ifndef __A20_LOGIC_OF_THE_STRUCTURE_H__
#define __A20_LOGIC_OF_THE_STRUCTURE_H__

extern void check_Value_is_Node(unsigned int from_node, unsigned int number, unsigned int type,int value);
extern void get_struct_JSON_HTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);

#endif // __A20_LOGIC_OF_THE_STRUCTURE_H__
