#include "Arduino.h"

/**********************************************************************************************************************/
/* Обработчики команд */
//--------------DHT-------------------------//
void commandsDht(WebServer &server) {
  float h = dht.getHumidity();
  float t = dht.getTemperature();

  if (gParamValue == 2){
    if (isnan(t) || isnan(h)) {
      server.print("Failed to read from DHT");
    } 
    else {
      server.print("H="); 
      server.print(h, 1);
      server.print(";");
      server.print("T="); 
      server.print(t, 1);
      server.print(";");
    }
    delay(2000);
  }
  else ErrorMessage(server);
}
//--------------DHT-------------------------//

void commandsSwitch(WebServer &server) {
  gParamValue = gParamValue-1; // т.к. нумерация в ардуино идет с 0 а у нас с 1
  /*
 if (gParamValue>=0 && gParamValue<Rele_Count) {
  /*
   if (digitalRead(Pin_Count_Port_Rele[gParamValue]) == HIGH)
   
   digitalWrite(Pin_Count_Port_Rele[gParamValue],ON );  
   else if (digitalRead(Pin_Count_Port_Rele[gParamValue]) == LOW)
   digitalWrite(Pin_Count_Port_Rele[gParamValue],OFF ); 
   
   }
   
   
   else ErrorMessage(server); 
   */
}


void commands_Read_Sensor(WebServer &server) {

  if (gParamValue>0 && Read_State_Sensors (00,gParamValue) == 00)ErrorMessage(server);
  Streamprint(server,"\{\"s%d\":\"",gParamValue);
  server.print(Read_State_Sensors (00,gParamValue));
  Streamprint(server,"\"}\n");

}

void commandsAn(WebServer &server) {
  if (gParamValue>0 && Read_State_Sensors (00,gParamValue) == 00)ErrorMessage(server);
  Streamprint(server,"\{\"A%d\":\"",gParamValue);
  server.print(Read_State_Sensors (00,gParamValue));

  // Streamprint(server,"\"}\n");    Streamprint(server,"\{\"A%d\":\"\%d\"}\n",gParamValue,State_Count_Port_Analog_IN[gParamValue]);

}
void commandsDig(WebServer &server) {
  if (gParamValue>0 && Read_State_Sensors (00,gParamValue) == 00)ErrorMessage(server);
  Streamprint(server,"\{\"D%d\":\"",gParamValue);
  server.print(Read_State_Sensors (00,gParamValue));
  Streamprint(server,"\"}\n");

  //Streamprint(server,"\{\"D%d\":\"\%d\"}\n",gParamValue,State_Count_Port_Digital_IN[gParamValue]);

}

void commandsOn(WebServer &server) {
  // gParamValue = gParamValue-1; // т.к. нумерация в ардуино идет с 0 а у нас с 1  
  //  if (gParamValue>0 && Read_State_Sensors (00,gParamValue) == 00)ErrorMessage(server);
  if (gParamValue>0 && lst.countElem == 0)ErrorMessage(server);

  Write_State_Sensors (00, gParamValue, ON);
  /*
  if (gParamValue>=0 && gParamValue<=Rele_Count) {
   if ( eeprom_config_pin.Rele_Count_Pin[gParamValue] == HIGH ){
   //             EEPROM.write(Adr_Save_EEPROM_Rele+gParamValue,HIGH);
   eeprom_config_pin.State_Count_Port_Rele[gParamValue] = ON;
   EEPROM_writeAnything(Adr_Save_EEPROM_Rele, eeprom_config_pin);
   }
   digitalWrite(Pin_Count_Port_Rele[gParamValue], ON);
   }
   else ErrorMessage(server);
   
   */
}

void commandsOff(WebServer &server) {
  // gParamValue = gParamValue-1; // т.к. нумерация в ардуино идет с 0 а у нас с 1 
  if (gParamValue>0 && lst.countElem == 0)ErrorMessage(server);
  Write_State_Sensors (00, gParamValue, OFF); 
  /*
  if (gParamValue>=0 && gParamValue<=Rele_Count) {
   if (eeprom_config_pin.Rele_Count_Pin[gParamValue] == HIGH) {
   //             EEPROM.write(Adr_Save_EEPROM_Rele+gParamValue,HIGH);
   eeprom_config_pin.State_Count_Port_Rele[gParamValue] = OFF;
   EEPROM_writeAnything(Adr_Save_EEPROM_Rele, eeprom_config_pin);
   }
   digitalWrite(Pin_Count_Port_Rele[gParamValue], OFF);
   }
   else ErrorMessage(server);
   */
}

void commandsClick(WebServer &server) {
  // gParamValue = gParamValue-1; // т.к. нумерация в ардуино идет с 0 а у нас с 1  
  if (gParamValue>0 && lst.countElem == 0)ErrorMessage(server);
  Write_State_Sensors (00, gParamValue, ON);
  delay(delayClick);
  Write_State_Sensors (00, gParamValue, OFF);
  /*
  if (gParamValue>=0 && gParamValue<=Rele_Count) {
   //      State_Count_Port_Rele[gParamValue] = ON;
   if ( eeprom_config_pin.Rele_Count_Pin[gParamValue] == HIGH ){
   //             EEPROM.write(Adr_Save_EEPROM_Rele+gParamValue,HIGH);
   eeprom_config_pin.State_Count_Port_Rele[gParamValue] = ON;
   EEPROM_writeAnything(Adr_Save_EEPROM_Rele, eeprom_config_pin);
   }
   digitalWrite(Pin_Count_Port_Rele[gParamValue], ON);    
   delay(delayClick);
   //      State_Count_Port_Rele[gParamValue] = OFF;
   if (eeprom_config_pin.Rele_Count_Pin[gParamValue] == HIGH) {
   //             EEPROM.write(Adr_Save_EEPROM_Rele+gParamValue,HIGH);
   eeprom_config_pin.State_Count_Port_Rele[gParamValue] = OFF;
   EEPROM_writeAnything(Adr_Save_EEPROM_Rele, eeprom_config_pin);
   }
   digitalWrite(Pin_Count_Port_Rele[gParamValue], OFF); 
   }
   else ErrorMessage(server);
   */
}

void commandsLClick(WebServer &server) {
  // gParamValue = gParamValue-1; // т.к. нумерация в ардуино идет с 0 а у нас с 1
  if (Read_State_Sensors (00,gParamValue) == 00)ErrorMessage(server);
  Write_State_Sensors (00, gParamValue, ON);
  delay(delayLClick);
  Write_State_Sensors (00, gParamValue, OFF); 
  /*
  if (gParamValue>=0 && gParamValue<=Rele_Count) {
   gParamValue=gParamValue-1;
   //      State_Count_Port_Rele[gParamValue] = ON; 
   if ( eeprom_config_pin.Rele_Count_Pin[gParamValue] == HIGH ){
   //             EEPROM.write(Adr_Save_EEPROM_Rele+gParamValue,HIGH);
   eeprom_config_pin.State_Count_Port_Rele[gParamValue] = ON;
   EEPROM_writeAnything(Adr_Save_EEPROM_Rele, eeprom_config_pin);
   }
   digitalWrite(Pin_Count_Port_Rele[gParamValue], ON);    
   delay(delayLClick);
   //      State_Count_Port_Rele[gParamValue] = OFF;  
   if (eeprom_config_pin.Rele_Count_Pin[gParamValue] == HIGH) {
   //             EEPROM.write(Adr_Save_EEPROM_Rele+gParamValue,HIGH);
   eeprom_config_pin.State_Count_Port_Rele[gParamValue] = OFF;
   EEPROM_writeAnything(Adr_Save_EEPROM_Rele, eeprom_config_pin);
   }
   digitalWrite(Pin_Count_Port_Rele[gParamValue], OFF); 
   }
   else ErrorMessage(server);
   */
}

void commandsStatus(WebServer &server) {

  if  (strcmp(gParamBuffer,  "RELE") == 0) { // выдать состояние всех пинов

    Streamprint(server,"{"); 
    for (int thisPin = 1; thisPin <= 16; thisPin++)  {
      Streamprint(server,"\"P%d\":\"",thisPin);
      server.print(Read_State_Sensors (00,thisPin));
      Streamprint(server,"\"",thisPin);
      if (thisPin != 16) Streamprint(server,",");

    }
    Streamprint(server,"}"); 
  }

  else if (strcmp(gParamBuffer,  "AN") == 0){
    Streamprint(server,"{"); 
    for (int thisPin = 17; thisPin <= 30; thisPin++)  {
      Streamprint(server,"\"A%d\":\"",thisPin-16);
      server.print(Read_State_Sensors (00,thisPin));
      Streamprint(server,"\"",thisPin);
      if (thisPin != 30) Streamprint(server,",");

    }
    Streamprint(server,"}"); 
  }

  else if (strcmp(gParamBuffer,  "DIG") == 0){
    Streamprint(server,"{"); 
    for (int thisPin = 31; thisPin <= 38; thisPin++)  {
      Streamprint(server,"\"D%d\":\"",thisPin-30);
      server.print(Read_State_Sensors (00,thisPin));
      Streamprint(server,"\"",thisPin);
      if (thisPin != 38) Streamprint(server,",");

    }
    Streamprint(server,"}"); 
  }  
  /*  
   else if (strcmp(gParamBuffer,  "DHT") == 0){
   //----------------------DHT------------------------------------------
   float h = dht.getHumidity();
   float t = dht.getTemperature();
   
   if (isnan(t) || isnan(h)) {
   server.print("Failed to read DHT");
   } 
   else {
   server.print("H="); 
   server.print(h, 1);
   server.print(";");
   server.print("T="); 
   server.print(t, 1);
   server.print(";");
   }  
   }
   
   else { // выдать состояние только 1 пина
   if (gParamValue>=0 && gParamValue<=Rele_Count) {
   int st=digitalRead(Pin_Count_Port_Rele[gParamValue]);          
   
   Streamprint(server,"\{\"P%d\":\"\%d\"}\n",Web_Count_Port_Rele[gParamValue],st);
   }
   else ErrorMessage(server);
   }
   */
}

void commandsHelp(WebServer &server) {
  int idx;
  for (idx = 0; gCommandTable[idx].name != NULL; idx++) {
    server.print(gCommandTable[idx].name);
    server.print("<br>");
  }
}


/*

 Чтение значений значения с сенсора пример : http://192.168.10.100/wifi?2=1,6,0
 2 - номер ноды
 1 - команда (1 - чтение , 2 - запись)
 6 - номер датчика
 0 - не имеет значения
 
 Запись значений значения с сенсора пример : http://192.168.10.100/wifi?20=2,16,1
 20 - номер ноды
 2  - команда (1 - чтение , 2 - запись)
 16 - номер датчика
 1  - записываемое значение
 
 */

void commandsWifi(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete) {
  server.httpSuccess();  // this line sends the standard "we're all OK" headers back to the browser

#define NAMELEN 3
#define VALUELEN 16

  URLPARAM_RESULT rc;
  char name[NAMELEN];
  char value[VALUELEN];

  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */
  if (type == WebServer::HEAD)
    return;

  if (strlen(url_tail))
  {
    while (strlen(url_tail))
      //   server.print(url_tail);
      rc = server.nextURLparam(&url_tail, name, NAMELEN, value, VALUELEN);
    if (rc == URLPARAM_EOS){
      // server.printP(Params_end);
    }
    else
    {
      /*
      server.print(name);
       server.print(" = '");
       server.print(value);
       server.print("'<br>\n");
       */
      char Command[3], *Command_End;
      char Sensor[3], *Sensor_End;
      char Values[5], *Values_End;
      sscanf(value, "%[^','],%[^','],%s", &Command, &Sensor, &Values);

      int name_temp = atoi(name);
      int Command_temp = atoi(Command);
      int Sensor_temp = atoi(Sensor);
      int Values_temp = atoi(Values);
      /*
      server.print(name_temp);
       server.print(" , ");
       server.print(Command_temp);
       server.print(" , ");
       server.print(Sensor_temp);
       server.print(" , ");
       server.print(Values_temp);
       server.print(" - ");
       server.print(Command);
       server.print(" , ");
       server.print(Sensor);
       server.print(" , ");
       server.print(Values);     
       server.print(" "); 
       */
      bool name_ok = name_temp >= 1 && name_temp <= 250;
      bool Command_ok = Command_temp >= 1 && Command_temp <= 2;
      bool Sensor_ok = Sensor_temp >= 1 && Sensor_temp <= 250;
      bool Values_ok = Values_temp >= -32767 && Values_temp <= 32767;

      if (name_ok &&Command_ok && Sensor_ok && Values_ok)
      {
        if (Command_temp == 1) //READ
        {

          Element *pTemp = lst.pHead;	                                     //создаем временный элемент
          pTemp = lst.Search(name_temp, Sensor_temp,0,2);                    //поиск по номеру сенсора
          //server.println(check_send_value (pTemp->sensor_value, pTemp->sensor_type));
/*
{"W1":"8,18,8.01 "}
W1 - номер ноды
8  - номер сенсора
18 - ID датчика
8.91 - показания сенсора
*/
          Streamprint(server,"\{\"W%d\":[%d,%d,",pTemp->modul_ID,pTemp->sensor_numder,pTemp->sensor_type);
          server.println(check_send_value (pTemp->sensor_value, pTemp->sensor_type));
          Streamprint(server,"]}\n",pTemp->sensor_type);
        }
        else if (Command_temp == 2) //WRITE
        {
          Element *pTemp = lst.pHead;	                                     //создаем временный элемент
          pTemp = lst.Search(name_temp, Sensor_temp,0,2);                    //поиск по номеру сенсора



          if ( name_temp == pTemp->modul_ID && Sensor_temp == pTemp->sensor_numder)    //если номер ноды и номер сенсора есть в общес списке
          {
            bool sensor_type_ok = pTemp->sensor_type == 1 || pTemp->sensor_type == 19 || pTemp->sensor_type == 20 || pTemp->sensor_type == 21
              || pTemp->sensor_type == 22 || pTemp->sensor_type == 23 || pTemp->sensor_type == 24 || pTemp->sensor_type == 25;
            
            bool RELE_ok = Command_temp == 2 && pTemp->sensor_type == 1 || pTemp->sensor_type == 19;
            if (sensor_type_ok)
            {
              if (RELE_ok){
                if ( Values_temp == 0 || Values_temp == 1) //проверка значения реле или кнопки
                {
                  Streamprint(server,"OK");
                  Check_Send_Message(name_temp,pTemp->sensor_type,Sensor_temp,Values_temp); //node,type,number,value
                }
                else Streamprint(server,"Ошибка: Не верное зн. реле или кнопки");
              }
              else{
                if (Values_temp == pTemp->sensor_value){
                  Streamprint(server,"Ошибка: Сенсор уже имеет такое значение");
                }
                else{
                  Streamprint(server,"OK");
                  Check_Send_Message(name_temp,pTemp->sensor_type,Sensor_temp,Values_temp); //node,type,number,value
                }
              }
            }
            else Streamprint(server,"Ошибка: Нельзя изменить значение сенсора");
          }
          else Streamprint(server,"Ошибка: Нет такого сенсора");
        }
      }
      else Streamprint(server,"Ошибка: Не верные данные");
    }


  }

}











