#ifndef __A55_HTML_GET_1WIRE_DEVICE
#define __A55_HTML_GET_1WIRE_DEVICE

extern void get1wireDevices(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
extern void get1wireDevicesHTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);

#endif
