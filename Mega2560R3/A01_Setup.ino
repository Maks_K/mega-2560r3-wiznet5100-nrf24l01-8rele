void setup() {
  // Для дебага будем выводить отладочные сообщения в консоль
  //TODO Убрать вывод в консоль "за дабаг" (т.е. вывод только если скимпилированно с поддержкой дебага)
  //SPI.setClockDivider(SPI_CLOCK_DIV2);
  //Устанавливает делитель частоты синхронизации SPI к частоте контроллера. 
  //Доступны следующие делители: 2, 4, 8, 16, 32, 64 или 128. Значение по умолчанию равно SPI_CLOCK_DIV4, 
  //одна четверть от частоты контроллера. 
  Serial.begin(57600);
  Serialprint("Start\n");

#if FASTADC
  // set prescale to 16
  sbi(ADCSRA,ADPS2) ;
  cbi(ADCSRA,ADPS1) ;
  cbi(ADCSRA,ADPS0) ;
#endif

  //--------------------------------Настраиваем таймеры---------------------------------------

  tmr1.event(&elapsed1);
  tmr2.event(&elapsed2);  
  tmr3.event(&elapsed3);
  tmr4.event(&elapsed4);  
  tmr5.event(&elapsed5);  
  tmr1.start();
  tmr2.start();
  tmr3.start();
  tmr4.start();
  tmr5.start();
  //--------------------------------Настраиваем таймеры---------------------------------------

  //--------------------------GSM-------------------------------------
#ifdef USED_GSM
  Serial1.begin(9600);
  Serial.println("GSM Shield testing.");
  if (gsm.begin(2400)){
    Serial.println("\nstatus=READY");
    started=true;  
  }
  else Serial.println("\nstatus=IDLE");

  if(started){
    //Enable this two lines if you want to send an SMS.
    //if (sms.SendSMS("3471234567", "Arduino SMS"))
    //Serial.println("\nSMS sent OK");
  }
#endif
  //--------------------------GSM-------------------------------------

  //--------------------------Inicialise_LiquidCrystal_I2C-------------------------------------
  lcd.begin(16,2);         // (20,4) initialize the lcd for 20 chars 4 lines and turn on backlight
  lcd.createChar(1, temp_cel);
  lcd.createChar(2, Rele_is_on);
  lcd.createChar(3, Rele_is_off);
  // ------- Quick 3 blinks of backlight  -------------
  for(int i = 0; i< 3; i++)
  {
    lcd.backlight();
    delay(250);
    lcd.noBacklight();
    delay(250);
  }
  lcd.backlight(); // finish with backlight on 

  lcd.setCursor(2,0);
  //lcd.print("Loading.");
  Streamprint(lcd,"Loading.");
  //--------------------------Inicialise_LiquidCrystal_I2C-------------------------------------
  printf_begin(); //start printf




  //-----------------------------< Работа с Картой памяти >------------------------------------
  // Задаем режим работы портов  
  pinMode(selectSd, OUTPUT);
  pinMode(selectEthernet, OUTPUT);

  digitalWrite(selectSd, HIGH);
  //-----------------------------< Работа с Картой памяти >------------------------------------


  //--------------------------------------Network_Setup--------------------------------------
  /* initialize the Ethernet adapter with the settings from eeprom */
  delay(200); // some time to settle
  verify_initialization_SD();
#define PREFIX ""
  setupNetwork();
  delay(200); // some time to settle

  //--------------------------------------Network_Setup--------------------------------------


  webserver->setDefaultCommand(&Request_HTML_SD); // дефолтная страница вывода (информация о контроллере)
  webserver->addCommand("command", &parsedRequest); // команды
  webserver->addCommand("state", &stateRequest); // выдать состояния всех устройств
  webserver->addCommand("wifi", &commandsWifi);
  webserver->addCommand("getdev", &get1wireDevices); // получить список устройств на 1-wire

  //--------------------------------------TEST--------------------------------------------
  webserver->addCommand("get_setupNet",&get_net_JSON_HTML_SD);// Получение данных в JSON Сетевые настройки
  webserver->addCommand("get_info",&get_info_JSON_HTML_SD);//Информационная страница

  webserver->addCommand("get_struct",&get_struct_JSON_HTML_SD);


  webserver->addCommand("json/static",&get_JSON_HTML_SD_Static);
  webserver->setFailureCommand(&fetchSD);
  //--------------------------------------TEST--------------------------------------------
  webserver->begin();
  Telnetserver->begin();

  Serialprint("server is at ");
  Serial.println(Ethernet.localIP());
  lcd.setCursor(2,0);
  Streamprint(lcd,"Loading..");
  delay(100);

  //---------------------------------------UDP Time------------------------------------------


  Udp.begin(localPort);
  update_Time_NTP();
  //---------------------------------------UDP Time------------------------------------------


  //--------------------------setup_nRF24L01----------------------------------------------

  // Какой узел мы?
  this_node = 00;

  //
  // Bring up the RF network
  // Вызовите RF сеть
  SPI.begin();
  radio.begin();
  network.begin(/*channel*/ 100, /*node address*/ this_node );

  lcd.setCursor(2,0);    
  //lcd.print("Loading..");
  Streamprint(lcd,"Loading...");
  delay(100);
  //--------------------------setup_nRF24L01----------------------------------------------*/

  for (int i=0; i < Number_of_Sensors; i++){
    check_Value_is_Node(00, i+1,Sensor_ID[i],0);
    check_Input_Output_Sensors (i+1); //стартовые настройки портов на меге, так же можно вызывать после изменения типа входа (работает только для меги)
  }
  for (int i=1; i < Number_of_Sensors; i++){
    Read_State_Sensors (00, i); //чтение состояния входа (применимо и для беспроводных клиентов )
  } 

  // Настройки 1-wire 
  sensors.begin(); // Инициализация шины 1-wire (для датчиков температуры)
  sensors.requestTemperatures(); // Перед каждым получением температуры надо ее запросить

  searchDevices();

  //--------------------------DHT---------------------------------------------- 
  //   Serial.println("DHTxx start!");
  //  pinMode(DHTPIN, INPUT);      // устанавливает режим работы - input
  //   dht.begin();
  //initialDHT();
  //--------------------------DHT----------------------------------------------  
  lcd.clear();
  delay(200);
  lcd.setCursor(0,0);
  Streamprint(lcd,"IP ");
  lcd.print(Ethernet.localIP());
  Print_LCD(Type_Screen_LCD);

}












