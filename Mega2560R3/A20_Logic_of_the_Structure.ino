#include "Arduino.h"

void check_Value_is_Node(unsigned int from_node, byte number, byte type,int value)
{
 from_node = dec2oct(from_node);


if (send_MDM(from_node, number,type,value))//вызов отправки данных в MDM
{
  //если все OK
  Streamprint(Serial,"Send MDM OK\n\r");
}
else
{
  Streamprint(Serial,"ERROR Send MDM FALSE\n\r");
}

if (type==240) Serial.println (value);

  if (lst.countElem == 0 && lst.Edit(from_node, number, type, value,1,hour(),minute(),second(),day(),month(),year()) == 0) {
    Streamprint(Serial,"Add NULL struct\n");
    lst.addToList(from_node,number,type,value,hour(),minute(),second(),day(),month(),year());
  }

  else if (lst.countElem != 0 && lst.Edit(from_node, number, type, value,1,hour(),minute(),second(),day(),month(),year()) == 0) {
    Streamprint(Serial,"Add struct\n");
    lst.addToList(from_node,number,type,value,hour(),minute(),second(),day(),month(),year());
  }
}



void get_struct_JSON_HTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  if (type == WebServer::POST)
  {
    server.httpFail();
    return;
  }

  server.httpSuccess("application/json");


  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */

  if (type == WebServer::HEAD)
    return;

    Element *pTemp = lst.pHead;		                           //создается временный элемент

  if (lst.pHead == NULL)			                           //если список пуст, то 
  {
    Streamprint(server,"NULL");	                           //выводим соответствующее сообщение
  }
  else					                           //если все такие он не пуст, то
  {
    unsigned int last_modul_ID=pTemp->modul_ID;

    Streamprint(server,"{\"n%d\":{", pTemp->modul_ID);
    while(pTemp != NULL)	                                   //пока врем. элем. не будет указывать на хвост
    {
      if(pTemp->modul_ID != last_modul_ID){
        Streamprint(server,",\"n%d\":{",pTemp->modul_ID);
      }
      else
      {

      }

      Streamprint(server,"\"s%d\":{\"id\":%d,\"value\":",pTemp->sensor_numder,pTemp->sensor_type);
      server.print(check_send_value (pTemp->sensor_value, pTemp->sensor_type));
      Streamprint(server,",");
      Streamprint(server,"\"time\":\"%d:%d:%d-%d.%d.%d\"",pTemp->hour_struct,pTemp->minute_struct,pTemp->second_struct,pTemp->day_struct,pTemp->month_struct,pTemp->year_struct);
 
      Streamprint(server,"}");

      last_modul_ID = pTemp->modul_ID;     
      pTemp = pTemp->next;

      if(pTemp->modul_ID != last_modul_ID) Streamprint(server,"}");
      else  Streamprint(server,",");


    }
    Streamprint(server,",\"s0\":{\"id\": 0,\"value\": 0}}");
  }
}





float check_send_value (int value, int type)
{
  float sensor_value_temp;
  if (type == 2 || type == 26 || type == 27 || type == 28 || type == 29 || type == 6
    || type == 7 ||  type == 16 || type == 18 || type == 31 || type == 32 || type == 33 || type == 240) //возвращаем наши значения флоат ))
  {
    sensor_value_temp = float(value)/float(100);
    /*
    Serial.print(value);
     Serial.print("=");
     Serial.println(sensor_value_temp);
     */
    return sensor_value_temp;
  }
  else if(type == 15)//BMP085 слишком большое значение для int
  {
    sensor_value_temp = float(value)/float(10);
    //sensor_value_temp = (float(value)+float(60000))/float(100);
    /*
    Serial.print(value);
     Serial.print("=");
     Serial.println(sensor_value_temp);
     */
    return sensor_value_temp;
  }

  else
  {
    return value;
  } 

}





/*****************************************************************************************************
 *Перед вызовом обязательно список сенсоров внести в список!!! иначе будет постоянная ошибка
 * byte Number_of_Sensors_Temp - номер сенсора
 * check_Input_Output_Sensors (10);
 * 10 сенсор из списка устанавливается его состояние из массива Sensor_PIN согласно типу сенсора из списка
 * если нет такого сенсора в списке то ошибка "NO Sensors"
 ******************************************************************************************************/
void check_Input_Output_Sensors (byte Number_of_Sensors_Temp){
  // Выставляем тип датчика (вход, выход или что еще )
  unsigned int value;
  Number_of_Sensors_Temp=Number_of_Sensors_Temp-1;//т.к. в массисе отсчкт с 0 а в списке сенсор начинается с 1

    Element *pTemp = lst.pHead;	                                     //создаем временный элемент
  pTemp = lst.Search(00, Number_of_Sensors_Temp+1,0,2); //поиск по номеру сенсора

  switch ( pTemp->sensor_type) {
    //switch (Sensor_ID[Number_of_Sensors_Temp]) {
  case 19:
    digitalWrite(Sensor_PIN[Number_of_Sensors_Temp], OFF);
    pinMode(Sensor_PIN[Number_of_Sensors_Temp], OUTPUT);
    break;
  case 4:
    break;
  case 5:
    pinMode(Sensor_PIN[Number_of_Sensors_Temp], INPUT); 
    digitalWrite(Sensor_PIN[Number_of_Sensors_Temp], HIGH);
    break;      
  default:
    // выполняется, если не выбрана ни одна альтернатива
    // default необязателен
    Serial.print ("NO Sensors");
  }

}
/*****************************************************************************************************/


/*****************************************************************************************************
 *
 * unsigned int from_node - номер ноды
 * byte Number_of_Sensors_Temp - номер сенсора
 * возвращает значение Serial.print(Read_State_Sensors (0,37));
 * если нет такого сенсора то ошибка "Error Nothing modul_ID || sensor_numde"
 ******************************************************************************************************/
unsigned int Read_State_Sensors (unsigned int from_node, byte Number_of_Sensors_Temp){
  //добавить проверку наличия датчика в списке
  // проверка соответсвия id списку  
  unsigned int value;
  Number_of_Sensors_Temp=Number_of_Sensors_Temp-1;
  Element *pTemp = lst.pHead;	                                     //создаем временный элемент
  pTemp = lst.Search(00, Number_of_Sensors_Temp+1,0,2);

  switch (pTemp->sensor_type) {//пока работает только для меги( нужно считывать id из списка)
  case 19://Обработка чтения Rele (или вызов функции)
    if (from_node == 0) value = digitalRead(Sensor_PIN[Number_of_Sensors_Temp]); //читаем значение
    //тут пишем обработчик для вывода данных если нода не мега
    break;
  case 4://Обработка чтения analog_in (или вызов функции)
    if (from_node == 0) value = analogRead(Sensor_PIN[Number_of_Sensors_Temp]); //читаем значение
    break;
  case 5://Обработка чтения Digital_in (или вызов функции)
    if (from_node == 0) value = digitalRead(Sensor_PIN[Number_of_Sensors_Temp]); //читаем значение
    break;      
  default:
    // выполняется, если не выбрана ни одна альтернатива
    // default необязателен
    return 0;
  }
  if (lst.Edit(from_node, Number_of_Sensors_Temp+1, pTemp->sensor_type, value,1,hour(),minute(),second(),day(),month(),year()) == 0)Streamprint(Serial,"Error Nothing modul_ID || sensor_numde\n");
  return  value;

}
/*****************************************************************************************************/


/*****************************************************************************************************
 *
 * unsigned int from_node - номер ноды
 * byte Number_of_Sensors_Temp - номер сенсора
 * int Sensor_value_temp - записываемое значение
 * если нет такого сенсора то ошибка "Error Nothing modul_ID || sensor_numde"
 ******************************************************************************************************/
unsigned int Write_State_Sensors (unsigned int from_node, byte Number_of_Sensors_Temp, int Sensor_value_temp){
  unsigned int value;
  Number_of_Sensors_Temp=Number_of_Sensors_Temp-1;
  Element *pTemp = lst.pHead;	                                     //создаем временный элемент
  pTemp = lst.Search(00, Number_of_Sensors_Temp+1,0,2);
  switch (pTemp->sensor_type) {//пока работает только для меги( нужно считывать id из списка)
  case 19://Обработка чтения Rele (или вызов функции)
    if (from_node == 0){
      digitalWrite(Sensor_PIN[Number_of_Sensors_Temp],Sensor_value_temp); //читаем значение
      value = digitalRead(Sensor_PIN[Number_of_Sensors_Temp]); //читаем значение
    }
    //тут пишем обработчик для вывода данных если нода не мега
    break;
    // case 4://Обработка чтения analog_in (или вызов функции)
    //   if (from_node == 0) value = analogWrite(Sensor_PIN[Number_of_Sensors_Temp]); //читаем значение
    //   break;
    // case 5://Обработка чтения Digital_in (или вызов функции)
    //   if (from_node == 0) value = digitalWrite(Sensor_PIN[Number_of_Sensors_Temp]); //читаем значение
    //    break;      
  default:
    // выполняется, если не выбрана ни одна альтернатива
    // default необязателен
    return 0;
  }
  if (lst.Edit(from_node, Number_of_Sensors_Temp+1, pTemp->sensor_type, Sensor_value_temp,1,hour(),minute(),second(),day(),month(),year()) == 0)Streamprint(Serial,"Error Nothing modul_ID || sensor_numde\n");
  return  value;
}



int dec2oct(int d) {
   int s = d < 0 ? -1 : 1;
   d = abs(d);
   int r = 0;
   int m = 1;
   do {
       r += (d%8)*m;
       m *= 10;
       d = d / 8;
   } while( d );
   return s*r;
}



int oct2dec(int d) {
   int s = d < 0 ? -1 : 1;
   d = abs(d);
   int r = 0;
   int m = 1;
   do {
       r += (d%10)*m;
       m *= 8;
       d = d / 10;
   } while( d );
   return s*r;
}




