#ifndef __A80_HTML_COMMAND_EXECUTION
#define __A80_HTML_COMMAND_EXECUTION

extern void commandsDht(WebServer &server);
extern void commandsSwitch(WebServer &server);
extern void commandsAn(WebServer &server);
extern void commandsDig(WebServer &server);
extern void commandsOn(WebServer &server);
extern void commandsOff(WebServer &server);
extern void commandsClick(WebServer &server);
extern void commandsLClick(WebServer &server);
extern void commandsStatus(WebServer &server);
extern void commandsHelp(WebServer &server);
extern void commandsWifi(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);

#endif
