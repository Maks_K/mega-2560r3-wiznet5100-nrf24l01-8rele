/*******************************************************************************************************
 * //"GET /objects/?object=sensorMovement1&op=m&m=statusChanged&status=%i HTTP/1.0"
 * //будет отправлен запрос на сервер вида /objects/?object=sensorMovement1&op=m&m=statusChanged&status=1
 * //(вызов метода statusChanged объекта sensorMovement1 с параметром status=1)
/*
 * взято из примера BATONS http://smartliving.ru/forum/viewtopic.php?f=8&t=73
 * sprintf(buf, "GET /objects/?object=sensorAmper&op=m&m=eleChanged&t=%0d.%d HTTP/1.0", (int)Irms, abs(Irms));
 * объект: sensorVolt,sensorAmper,sensorVall 
 * свойства: direction,Electric,updated,updatedTime
 * объекты: sensorAmper,sensorVatt,sensorVolt
 * код метода:
 * -----------------------------------------------------------------
 * //$params['t']
 * $old_electric=$this->getProperty('electric');
 * $t=round($params['t'],1);
 * $this->setProperty('electric',$t);
 * if ($t>$old_electric) {
 * $d=1;
 * } elseif ($t<$old_electric) {
 * $d=-1;
 * } else {
 * $d=0;
 * }
 * 
 * $this->setProperty('direction',$d);
 * $this->setProperty("updated",time());
 * $this->setProperty("updatedTime",date("H:i",time()));
 * ------------------------------------------------------------------
 * код оформления:
 * 
 * <font size="4" color="red" face="Arial">POWER MONITOR</font><font size="3" color="gray" face="Arial">
 * <br>
 * Напряжение: <font color="cyan">%sensorVolt.Electric%</font>
 * Вольт
 * <br>
 * Ток: <font color="cyan">%sensorAmper.Electric%</font>
 * Ампер
 * <br>
 * Мощность: <font color="cyan">%sensorVatt.electric%</font>
 * Ватт
 * <br>
 * 
 * Последнее обновление: <font color="cyan">%sensorVatt.updatedTime%</font>
 * <br>
 ******************************************************************************************************************/

//           float tempC = sensors.getTempC(Termometers);
//          unsigned int temp1 = (tempC - (int)tempC) * 100;
//send_MDM("TempSensor0&op=m&m=tempChanged&&t=", temp1,tempC);

///objects/?object=dimmerHall&op=m&m=remoteStatusChanged&brightness=0
//где object - объект, m - метод&значение свойства.


bool send_MDM(unsigned int from_node, byte number, byte type,int value){

  const char *filename = "/net.ini";

  const size_t bufferLen = 80;
  char bu[bufferLen];

  IniFile ini(filename);

  if (!ini.open()){

    Streamprint(Serial,"Network ini file ");
    Serial.print(filename);
    Streamprint(Serial," does not exist\n\r");

    while(1); // !!!!!!!Тут нужно что-то по умолчания сделать 

  }

  Streamprint(Serial,"Network ini file exist\n\r");

  if (!ini.validate(bu, bufferLen)){
    Streamprint(Serial,"Network ini file ");
    Serial.print(ini.getFilename());
    Streamprint(Serial," not valid\n\r");

    // Добавить вывод ошибки

    while(1); // !!!!!!!Тут нужно что-то по умолчания сделать 
  }

  IPAddress rserver;


  ini.getIPAddress("network", "rserver", bu, bufferLen, rserver);

  // IPAddress rserver(192,168,0,250);
  SWITCH_TO_W5100;
  EthernetClient client;
  if (client.connect(rserver, 80)) 
  {

    //  sprintf(buf, "GET /objects/?object=TempSensor0&op=m&m=tempChanged&&t=%0d.%d HTTP/1.0", (int)tempC, abs(temp1));
    // client.println(buf);
    // client.println();
    // Serial.print("T0 : ");
    // Serial.print(tempC);
    sprintf(buf, "GET /objects/?object=%d%d%d&op=m&m=stateChanged&status=%d HTTP/1.0", from_node, number, type, value);
    client.println(buf);
    client.println();
    Streamprint(Serial,"T0 : %d%d%d - ", from_node, number, type);
    Streamprint(Serial,"%d",value);
  }  
  else {
    //   Serial.println("connection failed");
    Streamprint(Serial,"ERROR - connection failed MDM\n\r");
    client.stop();
    ALL_OFF; 
    return false;
  }



  client.stop();
  ALL_OFF; 

}




///objects/?object=dimmerHall&op=m&m=remoteStatusChanged&brightness=0
//где object - объект, m - метод&значение свойства.

void Full_Send_MDM(){
  Element *pTemp = lst.pHead;	                                     //создаем временный элемент
  if (lst.pHead == NULL)
  {
    Streamprint(Serial,"Spisok pust\n");	                     //выводим соответствующее сообщение
  }
  else					                             //если все такие он не пуст, то
  {
    while(pTemp != NULL)		                             //пока не указываем на хвост
    {
       unsigned int temp,temp2,temp3;
       int temp4;
      pTemp->modul_ID = temp;
        pTemp->sensor_numder = temp2;
        pTemp->sensor_type = temp3;
        pTemp->sensor_value = temp4;
        const char *message_object = temp2 + "_" + temp3;

        
//send_MDM(temp, message_object,temp4);
    }
  }

 // send_MDM(char *classes,unsigned int variable,float value)
  }


